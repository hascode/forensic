#!/bin/bash

if [ -z $HASCODE_SSH_CRED ]; then
  echo 'SSH target not set in env-var "HASCODE_SSH_CRED"'
  exit 1
fi

echo "generating html from asciidoc.."
asciidoctor -r asciidoctor-diagram index.adoc && \
echo "uploading index.html to $HASCODE_SSH_CRED.." && \
sftp "$HASCODE_SSH_CRED:app/forensic" <<EOT
rm assets/images/*
put -r assets
put index.html
quit
EOT

== Firewall

=== iptables

https://www.thegeekstuff.com/2012/08/iptables-log-packets/[source: Ramesh Natarajan/geekstuff.com]

==== iptables log dropped input packets

[source,bash]
----
iptables -N LOGGING
iptables -A INPUT -j LOGGING
iptables -A LOGGING -m limit --limit 2/min -j LOG --log-prefix "IPTables-Dropped: " --log-level 4
iptables -A LOGGING -j DROP
----

==== iptables log dropped outgoing packets

[source,bash]
----
iptables -N LOGGING
iptables -A OUTPUT -j LOGGING
iptables -A LOGGING -m limit --limit 2/min -j LOG --log-prefix "IPTables-Dropped: " --log-level 4
iptables -A LOGGING -j DROP
----

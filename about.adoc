== About

****

This is a collection of simple commands and procedures used when I am trying to investigate
in a dying system.footnote:[The most important thing is to get the system up and running again.
It's a good practice to have some shell-scripts at start to gather information without prolonging the server problems.].

This is the first draft, written down in a short time.
Therefore a link lot of editing is still needed and there may be errors or wrong descriptions.
****

[TIP]
====
Most information is targeted at these topics/environments:

* `Linux`
* `Java`
* `nginx`/`Apache`
* `Postgres`/`MySQL`
====

Gathering information is one thing - the other is the interpretation and building hypothesis and falsifying them avoiding
the common cognitive bias.footnote:[For a list of cognitive bias, see Wikipedia https://en.wikipedia.org/wiki/List_of_cognitive_biases.].

[quote,Ron Weinstein]
____
A Fool with a Tool is Still a Fool
____

If you, the humble reader may want to share additional information like snipppets, tools or
workflows, please feel free to use the contact form on my blog here: http://www.hascode.com/[www.hascode.com].

.About me
****

[.float-group]
--
image::head.png[float="left"]
I am a software engineer highly interested in agile software development, lean development,
software architecture and continuous learning and curiosity :)


Please feel free to visit
http://www.hascode.com[my blog] or http://www.micha-kops.com/[my website] for other articles,
publications, projects or references of mine.

For legal information please skip to the <<legal-imprint, imprint section>>.
--

****

